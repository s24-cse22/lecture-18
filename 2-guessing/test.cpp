#include <igloo/igloo.h>
#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace igloo;

std::string exec(std::string command) {
   char buffer[128];
   std::string result = "";

   // Open pipe to file
   FILE* pipe = popen((command + " 2>&1").c_str(), "r");
   if (!pipe) {
      return "popen failed!";
   }

   // read till end of process:
   while (!feof(pipe)) {

      // use buffer to read and add to result
      if (fgets(buffer, 128, pipe) != NULL)
         result += buffer;
   }

   pclose(pipe);
   result.erase(result.find_last_not_of(" \t\n\r\f\v") + 1);
   return result;
}

struct OutputParser{
    std::vector<std::string> result;

    OutputParser(std::string output){
        std::stringstream ss(output);
        std::string line;
        while (getline(ss, line)){
            if (line != ""){
                result.push_back(line);
            }
        }
    }
};


Context(TestProgram) {
    static void TearDownContext() {
        system("rm -rf temp");
    }

    Spec(Test_10_7){
        exec("g++ main.cpp -o temp -Itestlib -DSEQ='(int[]){7}' -DMIN='(int[]){1}' -DMAX='(int[]){10}'");
        std::string raw = exec("echo 10 5 7 | ./temp");
        std::vector<std::string> actual = OutputParser(raw).result;
        std::vector<std::string> expected = {
            "Enter guess [1..10]: Go higher",
            "Enter guess [1..10]: ",
            "It requires 4 attempts to guarantee success. You got lucky and guessed it in 2.",
        };

        Assert::That(actual, EqualsContainer(expected));
    }

    Spec(Test_100_42){
        exec("g++ main.cpp -o temp -Itestlib -DSEQ='(int[]){42}' -DMIN='(int[]){1}' -DMAX='(int[]){100}'");
        std::string raw = exec("echo 100 50 25 40 45 44 43 42 | ./temp");
        std::vector<std::string> actual = OutputParser(raw).result;
        std::vector<std::string> expected = {
            "Enter guess [1..100]: Go lower",
            "Enter guess [1..100]: Go higher",
            "Enter guess [1..100]: Go higher",
            "Enter guess [1..100]: Go lower",
            "Enter guess [1..100]: Go lower",
            "Enter guess [1..100]: Go lower",
            "Enter guess [1..100]: ",
            "You got it in 7 attempts, which is the minimum number of attempts required to guarantee success.",
        };

        Assert::That(actual, EqualsContainer(expected));
    }

    Spec(Test_100_889){
        exec("g++ main.cpp -o temp -Itestlib -DSEQ='(int[]){889}' -DMIN='(int[]){1}' -DMAX='(int[]){1000}'");
        std::string raw = exec("echo 1000 500 700 900 800 850 875 890 880 885 888 889 | ./temp");
        std::vector<std::string> actual = OutputParser(raw).result;
        std::vector<std::string> expected = {
            "Enter guess [1..1000]: Go higher",
            "Enter guess [1..1000]: Go higher",
            "Enter guess [1..1000]: Go lower",
            "Enter guess [1..1000]: Go higher",
            "Enter guess [1..1000]: Go higher",
            "Enter guess [1..1000]: Go higher",
            "Enter guess [1..1000]: Go lower",
            "Enter guess [1..1000]: Go higher",
            "Enter guess [1..1000]: Go higher",
            "Enter guess [1..1000]: Go higher",
            "Enter guess [1..1000]: ",
            "You got it in 11 attempts, but it could have been done in 10.",
        };

        Assert::That(actual, EqualsContainer(expected));
    }


    Spec(Test_10000_1){
        exec("g++ main.cpp -o temp -Itestlib -DSEQ='(int[]){7}' -DMIN='(int[]){1}' -DMAX='(int[]){10000}'");
        std::string raw = exec("echo 10000 5000 2500 1000 500 250 100 | ./temp");
        std::vector<std::string> actual = OutputParser(raw).result;
        std::vector<std::string> expected = {
            "Enter guess [1..10000]: Go lower",
            "Enter guess [1..10000]: Go lower",
            "Enter guess [1..10000]: Go lower",
            "Enter guess [1..10000]: Go lower",
            "Enter guess [1..10000]: Go lower",
            "Enter guess [1..10000]: Go lower",
            "Enter guess [1..10000]: ",
            "Better luck next time.",
            "The number was 7.",
        };

        Assert::That(actual, EqualsContainer(expected));
    }

Spec(Test_1_Invalid){
        exec("g++ main.cpp -o temp -Itestlib -DSEQ='(int[]){1}' -DMIN='(int[]){1}' -DMAX='(int[]){1}'");
        std::string raw = exec("echo 1 | ./temp");
        std::vector<std::string> actual = OutputParser(raw).result;
        std::vector<std::string> expected = {
            "Invalid upper limit."
        };

        Assert::That(actual, EqualsContainer(expected));
    }

};


int main(int argc, const char* argv[]){
    TestRunner::RunAllTests(argc, argv);
}