#include <iostream>
#include <ucm_random>
using namespace std;


void display_question(int num, int left, int right, int op){
        char opSymbol;
        if (op == 1){
            opSymbol = '+';
        }
        else if (op == 2){
            opSymbol = '-';
        }
        else if (op == 3){
            opSymbol = '*';
        }
        else if (op == 4){
            opSymbol = '/';
        }
        else if (op == 5){
            opSymbol = '%';
        }

        cout << num << ". " << left << " " << opSymbol << " " << right << " = ";
}

int calculate(int left, int right, int op){
    if (op == 1){
        return left + right;
    }
    else if (op == 2){
        return left - right;
    }
    else if (op == 3){
        return left * right;
    }
    else if (op == 4){
        return left / right;
    }
    else if (op == 5){
        return left % right;
    }
    else{
        return 0;
    }
}

int main() {

    RNG rng;

    int attempts[5] = {0,0,0,0,0};

    for (int i = 0; i < 5; i++){
        int left;
        int right; 
        int op;

        left = rng.get(1, 15);
        right = rng.get(1, 15);
        op = rng.get(1, 5);

        display_question(i+1, left, right, op);
        
        int expected = calculate(left, right, op);
        
        int answer;
        cin >> answer;

        attempts[i]++;

        while (answer != expected){
            display_question(i+1, left, right, op);
            cin >> answer;
            attempts[i]++;
        }

        cout << endl;
    }

    for(int i = 0; i < 5; i++){
        cout << "Question " << i+1 << " took you " << attempts[i] << " attempts." << endl;
    }

    return 0;
}