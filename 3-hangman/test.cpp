#include <igloo/igloo.h>
#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace igloo;

std::string exec(std::string command) {
   char buffer[128];
   std::string result = "";

   // Open pipe to file
   FILE* pipe = popen((command + " 2>&1").c_str(), "r");
   if (!pipe) {
      return "popen failed!";
   }

   // read till end of process:
   while (!feof(pipe)) {

      // use buffer to read and add to result
      if (fgets(buffer, 128, pipe) != NULL)
         result += buffer;
   }

   pclose(pipe);
   result.erase(result.find_last_not_of(" \t\n\r\f\v") + 1);
   return result;
}

struct OutputParser{
    std::vector<std::string> result;

    OutputParser(std::string output){
        std::stringstream ss(output);
        std::string line;
        while (getline(ss, line)){
            if (line != ""){
                result.push_back(line);
            }
        }
    }
};

Context(TestProgram) {
    static void SetUpContext(){
        exec("g++ main.cpp -o temp");
    }

    static void TearDownContext() {
        system("rm -rf temp");
    }

    Spec(Test_APPLE){
        std::string raw = exec("echo 'APPLE A P L E' | ./temp");
        std::vector<std::string> actual = OutputParser(raw).result;
        std::vector<std::string> expected = {
            "Enter a letter: A _ _ _ _",
            "Letters: A",
            "Enter a letter: A P P _ _",
            "Letters: A, P",
            "Enter a letter: A P P L _",
            "Letters: A, P, L",
            "Enter a letter: A P P L E",
            "Great job!",
        };

        Assert::That(actual, EqualsContainer(expected));
    }

    Spec(Test_MACBOOK){
        std::string raw = exec("echo 'MACBOOK a b c d e f g h i j k l m n o' | ./temp");
        std::vector<std::string> actual = OutputParser(raw).result;
        std::vector<std::string> expected = {
            "Enter a letter: _ A _ _ _ _ _",
            "Letters: A",
            "Enter a letter: _ A _ B _ _ _",
            "Letters: A, B",
            "Enter a letter: _ A C B _ _ _",
            "Letters: A, B, C",
            "Enter a letter: _ A C B _ _ _",
            "Letters: A, B, C, D",
            "Enter a letter: _ A C B _ _ _",
            "Letters: A, B, C, D, E",
            "Enter a letter: _ A C B _ _ _",
            "Letters: A, B, C, D, E, F",
            "Enter a letter: _ A C B _ _ _",
            "Letters: A, B, C, D, E, F, G",
            "Enter a letter: _ A C B _ _ _",
            "Letters: A, B, C, D, E, F, G, H",
            "Enter a letter: _ A C B _ _ _",
            "Letters: A, B, C, D, E, F, G, H, I",
            "Enter a letter: _ A C B _ _ _",
            "Letters: A, B, C, D, E, F, G, H, I, J",
            "Enter a letter: _ A C B _ _ K",
            "Letters: A, B, C, D, E, F, G, H, I, J, K",
            "Enter a letter: _ A C B _ _ K",
            "Letters: A, B, C, D, E, F, G, H, I, J, K, L",
            "Enter a letter: M A C B _ _ K",
            "Letters: A, B, C, D, E, F, G, H, I, J, K, L, M",
            "Enter a letter: M A C B _ _ K",
            "Letters: A, B, C, D, E, F, G, H, I, J, K, L, M, N",
            "Enter a letter: M A C B O O K",
            "Great job!",
        };

        Assert::That(actual, EqualsContainer(expected));
    }

    Spec(Test_golf){
        std::string raw = exec("echo 'golf F L O G' | ./temp");
        std::vector<std::string> actual = OutputParser(raw).result;
        std::vector<std::string> expected = {
            "Enter a letter: _ _ _ F",
            "Letters: F",
            "Enter a letter: _ _ L F",
            "Letters: F, L",
            "Enter a letter: _ O L F",
            "Letters: F, L, O",
            "Enter a letter: G O L F",
            "Great job!",
        };

        Assert::That(actual, EqualsContainer(expected));
    }

    Spec(Test_iPhone){
        std::string raw = exec("echo 'iPhone a B c D e F g H i J k L m N' | ./temp");
        std::vector<std::string> actual = OutputParser(raw).result;
        std::vector<std::string> expected = {
            "Enter a letter: _ _ _ _ _ _",
            "Letters: A",
            "Enter a letter: _ _ _ _ _ _",
            "Letters: A, B",
            "Enter a letter: _ _ _ _ _ _",
            "Letters: A, B, C",
            "Enter a letter: _ _ _ _ _ _",
            "Letters: A, B, C, D",
            "Enter a letter: _ _ _ _ _ E",
            "Letters: A, B, C, D, E",
            "Enter a letter: _ _ _ _ _ E",
            "Letters: A, B, C, D, E, F",
            "Enter a letter: _ _ _ _ _ E",
            "Letters: A, B, C, D, E, F, G",
            "Enter a letter: _ _ H _ _ E",
            "Letters: A, B, C, D, E, F, G, H",
            "Enter a letter: I _ H _ _ E",
            "Letters: A, B, C, D, E, F, G, H, I",
            "Enter a letter: I _ H _ _ E",
            "Letters: A, B, C, D, E, F, G, H, I, J",
            "Enter a letter: I _ H _ _ E",
            "Letters: A, B, C, D, E, F, G, H, I, J, K",
            "Enter a letter: I _ H _ _ E",
            "Letters: A, B, C, D, E, F, G, H, I, J, K, L",
            "Enter a letter: I _ H _ _ E",
            "Letters: A, B, C, D, E, F, G, H, I, J, K, L, M",
            "Enter a letter: I _ H _ N E",
            "Letters: A, B, C, D, E, F, G, H, I, J, K, L, M, N",
            "Enter a letter: ",
            "Better luck next time!",
            "The word was: IPHONE",
        };

        Assert::That(actual, EqualsContainer(expected));
    }

    Spec(Test_Bobcats){
        std::string raw = exec("echo 'Bobcats b b s t c a a a o' | ./temp");
        std::vector<std::string> actual = OutputParser(raw).result;
        std::vector<std::string> expected = {
            "Enter a letter: B _ B _ _ _ _",
            "Letters: B",
            "Enter a letter: B _ B _ _ _ _",
            "Letters: B",
            "Enter a letter: B _ B _ _ _ S",
            "Letters: B, S",
            "Enter a letter: B _ B _ _ T S",
            "Letters: B, S, T",
            "Enter a letter: B _ B C _ T S",
            "Letters: B, S, T, C",
            "Enter a letter: B _ B C A T S",
            "Letters: B, S, T, C, A",
            "Enter a letter: B _ B C A T S",
            "Letters: B, S, T, C, A",
            "Enter a letter: B _ B C A T S",
            "Letters: B, S, T, C, A",
            "Enter a letter: B O B C A T S",
            "Great job!",
        };

        Assert::That(actual, EqualsContainer(expected));
    }

    Spec(Test_SpOnGeBoB){
        std::string raw = exec("echo 'SpOnGeBoB a E i O u S g t g p g' | ./temp");
        std::vector<std::string> actual = OutputParser(raw).result;
        std::vector<std::string> expected = {
            "Enter a letter: _ _ _ _ _ _ _ _ _",
            "Letters: A",
            "Enter a letter: _ _ _ _ _ E _ _ _",
            "Letters: A, E",
            "Enter a letter: _ _ _ _ _ E _ _ _",
            "Letters: A, E, I",
            "Enter a letter: _ _ O _ _ E _ O _",
            "Letters: A, E, I, O",
            "Enter a letter: _ _ O _ _ E _ O _",
            "Letters: A, E, I, O, U",
            "Enter a letter: S _ O _ _ E _ O _",
            "Letters: A, E, I, O, U, S",
            "Enter a letter: S _ O _ G E _ O _",
            "Letters: A, E, I, O, U, S, G",
            "Enter a letter: S _ O _ G E _ O _",
            "Letters: A, E, I, O, U, S, G, T",
            "Enter a letter: S _ O _ G E _ O _",
            "Letters: A, E, I, O, U, S, G, T",
            "Enter a letter: S P O _ G E _ O _",
            "Letters: A, E, I, O, U, S, G, T, P",
            "Enter a letter: S P O _ G E _ O _",
            "Letters: A, E, I, O, U, S, G, T, P",
            "Enter a letter: ",
            "Better luck next time!",
            "The word was: SPONGEBOB",
        };

        Assert::That(actual, EqualsContainer(expected));
    }

};

int main(int argc, const char* argv[]){
    TestRunner::RunAllTests(argc, argv);
}